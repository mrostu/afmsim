#ifndef micromagn_h
#define micromagn_h

#include <stdlib.h>

typedef struct {
    size_t Nx;
    size_t Ny;
    size_t N;
    double hx;
    double hy;
    double dx;
    double dy;
    double *xvec;
    double *yvec;
    double *l[3];
    double *dldx[3];
    double *dldy[3];
    double *d2ldx[3];
    double *d2ldy[3];
    double *Heff[3];
} Grid;

typedef struct {
    double kappaC;
    double kappaD;
    double kappaM;
    double hx;
    double hy;
    double hz;
} AFMParams;

typedef struct {
    size_t output_iter;
    int (*plot)(const Grid *);
} OutputStr;

Grid *  grid_init(double hx, double hy, double dx, double dy);
void    grid_free(Grid *g);
void    grid_norm_l(Grid *g);
int     grid_uniform_dist(Grid *g, double l0[3]);
int     grid_harmonic_dist(Grid *g, double kx, double ky);
int     grid_rand_dist(Grid *g);
void    grid_print_info(const Grid *grid);
size_t  get_index(size_t Nx, size_t Ny, long int i, long int j);
void    first_der(Grid *g);
void    second_der(Grid *g);
void    h_eff(Grid *g, const AFMParams *p);
int     mm_solve(Grid *g, AFMParams *p, double eps, const OutputStr *ostr);
void    mm_show_progress(size_t iter, double alpha, double energy,
            double max_bq_val);

int output_init(OutputStr *s, const size_t output_iter,
        int (*plot)(const Grid *));

#endif
