#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <gsl/gsl_rng.h>

#include "micromagn.h"
#include "berkov.h"
#include "plots.h"

#define SQR(x) x*x
#define SQR3(x, y, z) x*x + y*y + z*z
#define NORM3(x) sqrt(x[0]*x[0] + x[1]*x[1] + x[2]*x[2])
#define K11 0.83333333333e-2
#define K12 0.66666666666
#define K21 1.33333333333333
#define K22 2.5

static inline double norm3(double x, double y, double z) {
    return sqrt(x*x + y*y + z*z);
}
 
Grid * grid_init(double hx, double hy, double dx, double dy) {
    Grid *g;
    g = (Grid *) malloc(sizeof(Grid));

    g -> hx = hx;
    g -> hy = hy;
    g -> Nx = (size_t) (hx / dx);
    g -> Ny = (size_t) (hy / dy);
    g -> N  = g -> Nx * g -> Ny;
    g -> dx = (double) (hx / g -> Nx);
    g -> dy = (double) (hy / g -> Ny);
    g -> xvec = (double *) malloc(g->Nx*sizeof(double));
    g -> yvec = (double *) malloc(g->Ny*sizeof(double));

    size_t i, j;
    for (i = 0; i < 3; ++i) {
        g -> l[i]       = (double *) malloc(g->N*sizeof(double));
        g -> dldx[i]    = (double *) malloc(g->N*sizeof(double));
        g -> dldy[i]    = (double *) malloc(g->N*sizeof(double));
        g -> d2ldx[i]   = (double *) malloc(g->N*sizeof(double));
        g -> d2ldy[i]   = (double *) malloc(g->N*sizeof(double));
        g -> Heff[i]    = (double *) malloc(g->N*sizeof(double));
    }

    for (i = 0; i < g->Nx; ++i)
        g->xvec[i] = 0.5*g->dx + i*dx;
    for (i = 0; i < g->Ny; ++i)
        g->yvec[i] = 0.5*g->dy + i*dy;

    return g;
}

void grid_free(Grid *g) {
    free(g->xvec);
    free(g->yvec);
    size_t i;
    for (i = 0; i < 3; ++i) {
        free(g -> l[i]);
        free(g -> dldx[i]);
        free(g -> dldy[i]);
        free(g -> d2ldx[i]);
        free(g -> d2ldy[i]);
        free(g -> Heff[i]);
    }
    free(g);
}

void grid_norm_l(Grid *g) {
    size_t i;
    double norm;
    for (i = 0; i < g -> N; ++i) {
        norm = norm3(g->l[0][i], g->l[1][i], g->l[2][i]);
        g -> l[0][i] /= norm;
        g -> l[1][i] /= norm;
        g -> l[2][i] /= norm;
    }
}

int grid_uniform_dist(Grid *g, double l0[3]) {
    double norm = norm3(l0[0], l0[1], l0[2]);
    if (norm == 0.0)
        return 1;

    size_t i;
    const gsl_rng_type *T;
    gsl_rng *r;
    gsl_rng_env_setup();
    T = gsl_rng_default;
    r = gsl_rng_alloc(T);
    gsl_rng_set(r, (unsigned long int) time(NULL));

    for (i = 0; i < 3; ++i)
        l0[i] /= norm;
    for (i = 0; i < g -> N; ++i) {
        g -> l[0][i] = l0[0] + (2*gsl_rng_uniform(r) - 1.0);
        g -> l[1][i] = l0[1] + (2*gsl_rng_uniform(r) - 1.0);
        g -> l[2][i] = l0[2] + (2*gsl_rng_uniform(r) - 1.0);
        norm = norm3(g -> l[0][i], g -> l[1][i], g -> l[2][i]);
        g -> l[0][i] /= norm;
        g -> l[1][i] /= norm;
        g -> l[2][i] /= norm;
    }

    gsl_rng_free(r);

    return 0;
}

int grid_harmonic_dist(Grid *g, double kx, double ky) {
    if (kx == 0.0 && ky == 0.0)
        return 1;

    size_t i, j;
    double phi, phase;

    if (kx != 0.0)
        phi = atan(ky / kx);
    else
        phi = 2*atan(1.0) * ky/fabs(ky);

    for (i = 0; i < g -> Nx; ++i)
        for (j = 0; j < g -> Ny; ++j) {
            phase = kx * g->dx * i + ky * g->dy * j;
            g -> l[0][g->Nx*i+j] = cos(phi) * cos(phase);
            g -> l[1][g->Nx*i+j] = sin(phi) * cos(phase);
            g -> l[2][g->Nx*i+j] = sin(phase);
        }

    return 0;
}

int grid_rand_dist(Grid *g) {
    size_t i;
    double norm;

    const gsl_rng_type *T;
    gsl_rng *r;
    gsl_rng_env_setup();
    T = gsl_rng_default;
    r = gsl_rng_alloc(T);
    gsl_rng_set(r, (unsigned long int) time(NULL));

    for (i = 0; i < g -> N; ++i) {
        g -> l[0][i] = 2*gsl_rng_uniform(r) - 1.0;
        g -> l[1][i] = 2*gsl_rng_uniform(r) - 1.0;
        g -> l[2][i] = 2*gsl_rng_uniform(r) - 1.0;
        norm = norm3(g -> l[0][i], g -> l[1][i], g -> l[2][i]);
        g -> l[0][i] /= norm;
        g -> l[1][i] /= norm;
        g -> l[2][i] /= norm;
    }

    gsl_rng_free(r);

    return 0;
}

void grid_print_info(const Grid *grid) {
    float mem_size = (float)(3 * 5 * grid -> Nx * grid -> Ny)
        * (float) (sizeof(double) / 8);

    printf("\n========================== *** GRID INFORMATION *** ============================\n");
    printf("Number of \"x\" nodes: %lu\n", (long unsigned int) grid -> Nx);
    printf("Number of \"y\" nodes: %lu\n", (long unsigned int) grid -> Ny);
    printf("Approximate size in memory: %.2fM\n", (float) mem_size / 1000000.0);
    printf("================================================================================\n");
}

void braun_mod(double *bq, const Grid *g) {
    size_t i;
    for (i = 0; i < g -> N; ++i)
        bq[i] = norm3(
                    g->l[1][i] * g->Heff[2][i] - g->l[2][i] * g->Heff[1][i],
                    g->l[2][i] * g->Heff[0][i] - g->l[0][i] * g->Heff[2][i],
                    g->l[0][i] * g->Heff[1][i] - g->l[1][i] * g->Heff[0][i]
                );
}

size_t get_index(size_t Nx, size_t Ny, long int i, long int j) {
    size_t i1, j1;

    i1 = i >= 0 ? i%Nx : Nx + i;
    j1 = j >= 0 ? j%Nx : Ny + j;
    return Ny*i1 + j1;
}

double get_der(const double *l, double d, size_t Nx, size_t Ny,
        size_t i, size_t j, int ni) {
    size_t r1, r2, c, l1, l2;
    if (ni == 0) {
        r2 = get_index(Nx, Ny, (long int) i + 2, (long int) j);
        r1 = get_index(Nx, Ny, (long int) i + 1, (long int) j);
        l1 = get_index(Nx, Ny, (long int) i - 1, (long int) j);
        l2 = get_index(Nx, Ny, (long int) i - 2, (long int) j);
    } else {
        r2 = get_index(Nx, Ny, (long int) i, (long int) j + 2);
        r1 = get_index(Nx, Ny, (long int) i, (long int) j + 1);
        l1 = get_index(Nx, Ny, (long int) i, (long int) j - 1);
        l2 = get_index(Nx, Ny, (long int) i, (long int) j - 2);
    }
    
    return (-K11*l[r2] + K12*l[r1] - K12*l[l1] + K11*l[l2]) / d;
}

double get_der2(const double *l, double d, size_t Nx, size_t Ny,
        size_t i, size_t j, int ni) {
    size_t r1, r2, c, l1, l2;
    c  = get_index(Nx, Ny, (long int) i, (long int) j);
    if (ni == 0) {
        r2 = get_index(Nx, Ny, (long int) i + 2, (long int) j);
        r1 = get_index(Nx, Ny, (long int) i + 1, (long int) j);
        l1 = get_index(Nx, Ny, (long int) i - 1, (long int) j);
        l2 = get_index(Nx, Ny, (long int) i - 2, (long int) j);
    } else {
        r2 = get_index(Nx, Ny, (long int) i, (long int) j + 2);
        r1 = get_index(Nx, Ny, (long int) i, (long int) j + 1);
        l1 = get_index(Nx, Ny, (long int) i, (long int) j - 1);
        l2 = get_index(Nx, Ny, (long int) i, (long int) j - 2);
    }
    
    return (-K11*l[r2] + K21*l[r1] - K22*l[c] + K21*l[l1] - K11*l[l2]) / d;
}

void first_der(Grid *g) {
    size_t i, j, r1, r2, c, l1, l2;
    size_t Nx = g->Nx,
           Ny = g->Ny;
    for (i = 0; i < Nx; ++i)
        for (j = 0; j < Ny; ++j) {
            c  = get_index(Nx, Ny, i, j);
            /*
            r2 = get_index(Nx, Ny, i, j + 2);
            r1 = get_index(Nx, Ny, i, j + 1);
            l1 = get_index(Nx, Ny, i, j - 1);
            l2 = get_index(Nx, Ny, i, j - 2);
            */
            /*
            r2 = Nx*i + (j + 2) % Ny;
            r1 = Nx*i + (j + 1) % Ny;
            l2 = Nx*i + (j - 2) < 0 ? (Ny + j - 2) : (j - 2);
            l1 = Nx*i + (j - 1) < 0 ? (Ny + j - 1) : (j - 1);
            */
            g->dldy[0][c] = get_der(g->l[0], g->dy, Nx, Ny, i, j, 1);
            g->dldy[1][c] = get_der(g->l[1], g->dy, Nx, Ny, i, j, 1);
            g->dldy[2][c] = get_der(g->l[2], g->dy, Nx, Ny, i, j, 1);
            /*
            g->dldy[0][c] = (-K11*g->l[0][r2] + K12*g->l[0][r1]
                    - K12*g->l[0][l1] + K11*g->l[0][l2]) / g->dy;
            g->dldy[1][c] = (-K11*g->l[1][r2] + K12*g->l[1][r1]
                    - K12*g->l[1][l1] + K11*g->l[1][l2]) / g->dy;
            g->dldy[2][c] = (-K11*g->l[2][r2] + K12*g->l[2][r1]
                    - K12*g->l[2][l1] + K11*g->l[2][l2]) / g->dy;
            */
        }
    for (j = 0; j < Ny; ++j)
        for (i = 0; i < Nx; ++i) {
            c  = get_index(Nx, Ny, i, j);
            /*
            r2 = get_index(Nx, Ny, i + 2, j);
            r1 = get_index(Nx, Ny, i + 2, j);
            l1 = get_index(Nx, Ny, i - 1, j);
            l2 = get_index(Nx, Ny, i - 2, j);
            */
            /*
            r2 = Nx * ((i + 2) % Nx) + j;
            r1 = Nx * ((i + 1) % Nx) + j;
            l2 = Nx * ((i - 2) < 0 ? (Nx + i - 2) : (i - 2)) + j;
            l1 = Nx * ((i - 1) < 0 ? (Nx + i - 1) : (i - 1)) + j;
            */
            g->dldx[0][c] = get_der(g->l[0], g->dx, Nx, Ny, i, j, 0);
            g->dldx[1][c] = get_der(g->l[1], g->dx, Nx, Ny, i, j, 0);
            g->dldx[2][c] = get_der(g->l[2], g->dx, Nx, Ny, i, j, 0);
            /*
            g->dldx[0][c] = (-K11*g->l[0][r2] + K12*g->l[0][r1]
                    - K12*g->l[0][l1] + K11*g->l[0][l2]) / g->dx;
            g->dldx[1][c] = (-K11*g->l[1][r2] + K12*g->l[1][r1]
                    - K12*g->l[1][l1] + K11*g->l[1][l2]) / g->dx;
            g->dldx[2][c] = (-K11*g->l[2][r2] + K12*g->l[2][r1]
                    - K12*g->l[2][l1] + K11*g->l[2][l2]) / g->dx;
            */
        }
}

void second_der(Grid *g) {
    size_t i, j, r1, r2, c, l1, l2;
    size_t Nx = g->Nx,
           Ny = g->Ny;
    double dx = SQR(g->dx);
    double dy = SQR(g->dy);
    for (i = 0; i < Nx; ++i)
        for (j = 0; j < Ny; ++j) {
            c  = get_index(Nx, Ny, i, j);
            /*
            r2 = get_index(Nx, Ny, i, j + 2);
            r1 = get_index(Nx, Ny, i, j + 1);
            l1 = get_index(Nx, Ny, i, j - 1);
            l2 = get_index(Nx, Ny, i, j - 2);
            */
            /*
            r2 = Nx*i + (j + 2) % Ny;
            r1 = Nx*i + (j + 1) % Ny;
            l2 = Nx*i + (j - 2) < 0 ? (Ny + j - 2) : (j - 2);
            l1 = Nx*i + (j - 1) < 0 ? (Ny + j - 1) : (j - 1);
            */
            g->d2ldy[0][c] = get_der2(g->l[0], dy, Nx, Ny, i, j, 1);
            g->d2ldy[1][c] = get_der2(g->l[1], dy, Nx, Ny, i, j, 1);
            g->d2ldy[2][c] = get_der2(g->l[2], dy, Nx, Ny, i, j, 1);
            /*
            g->d2ldy[0][c] = (-K11*g->l[0][r2] + K21*g->l[0][r1]
                    - K22*g->l[0][c] + K21*g->l[0][l1]
                    - K11*g->l[0][l2]) / dy;
            g->d2ldy[1][c] = (-K11*g->l[1][r2] + K21*g->l[1][r1]
                    - K22*g->l[1][c] + K21*g->l[1][l1]
                    - K11*g->l[1][l2]) / dy;
            g->d2ldy[2][c] = (-K11*g->l[2][r2] + K21*g->l[2][r1]
                    - K22*g->l[2][c] + K21*g->l[2][l1]
                    - K11*g->l[2][l2]) / dy;
            */
        }
    for (j = 0; j < Ny; ++j)
        for (i = 0; i < Nx; ++i) {
            c  = get_index(Nx, Ny, i, j);
            /*
            r2 = get_index(Nx, Ny, i + 2, j);
            r1 = get_index(Nx, Ny, i + 2, j);
            l1 = get_index(Nx, Ny, i - 1, j);
            l2 = get_index(Nx, Ny, i - 2, j);
            */
            /*
            r2 = Nx * ((i + 2) % Nx) + j;
            r1 = Nx * ((i + 1) % Nx) + j;
            l2 = Nx * ((i - 2) < 0 ? (Nx + i - 2) : (i - 2)) + j;
            l1 = Nx * ((i - 1) < 0 ? (Nx + i - 1) : (i - 1)) + j;
            */
            g->d2ldx[0][c] = get_der2(g->l[0], dx, Nx, Ny, i, j, 0);
            g->d2ldx[1][c] = get_der2(g->l[1], dx, Nx, Ny, i, j, 0);
            g->d2ldx[2][c] = get_der2(g->l[2], dx, Nx, Ny, i, j, 0);
            /*
            g->d2ldx[0][c] = (-K11*g->l[0][r2] + K21*g->l[0][r1]
                    - K22*g->l[0][c] + K21*g->l[0][l1]
                    - K11*g->l[0][l2]) / dx;
            g->d2ldx[1][c] = (-K11*g->l[1][r2] + K21*g->l[1][r1]
                    - K22*g->l[1][c] + K21*g->l[1][l1]
                    - K11*g->l[1][l2]) / dx;
            g->d2ldx[2][c] = (-K11*g->l[2][r2] + K21*g->l[2][r1]
                    - K22*g->l[2][c] + K21*g->l[2][l1]
                    - K11*g->l[2][l2]) / dx;
            */
        }
}

double afm_energy(const Grid *g, const AFMParams *p) {
    double E = 0.0;
    size_t i;
    for (i = 0; i < g -> N; ++i)
        E += 0.5*(SQR3(g->dldx[0][i], g->dldx[1][i], g->dldx[2][i])
                + SQR3(g->dldy[0][i], g->dldy[1][i], g->dldy[2][i]))
                + g->l[0][i] * g->dldx[2][i] - g->l[2][i] * g->dldx[0][i]
                + g->l[1][i] * g->dldy[2][i] - g->l[2][i] * g->dldy[1][i]
                - 0.5*(p->kappaC - p->kappaD)*SQR(g->l[2][i])
                + 0.5*p->kappaM*SQR(p->hx * g->l[0][i] + p->hy * g->l[1][i]
                    + p->hy * g->l[2][i]) - sqrt(p->kappaD * p->kappaM)
                * (p->hx * g->l[1][i] - p->hy * g->l[0][i]);
    return E / g -> N;
}

void h_eff(Grid *g, const AFMParams *p) {
    size_t i;
    for (i = 0; i < g -> N; ++i) {
        g->Heff[0][i] = g->d2ldx[0][i] + g->d2ldy[0][i]
            - 2.0 * g->dldx[2][i] - p->kappaM * SQR(p->hx) * g->l[0][i]
            - sqrt(p->kappaD * p->kappaM) * p->hy;

        g->Heff[1][i] = g->d2ldx[1][i] + g->d2ldy[1][i]
            - 2.0 * g->dldy[2][i] - p->kappaM * SQR(p->hy) * g->l[1][i]
            + sqrt(p->kappaD * p->kappaM) * p->hx;

        g->Heff[2][i] = g->d2ldx[2][i] + g->d2ldy[2][i]
            + 2.0 * g->dldx[0][i] + 2.0 * g->dldy[1][i]
            + (p->kappaC - p->kappaD - p->kappaM * SQR(p->hz)) * g->l[2][i];
    }
}

int mm_criterion(const Grid *g, double eps, double *max_val) {
    double *bq = (double *) malloc(g->N * sizeof(double));
    braun_mod(bq, g);
    size_t i;
    double max = 0.1*eps;
    for (i = 0; i < g->N; ++i)
        if (max < bq[i])
            max = bq[i];
    free(bq);

    *max_val = max;
    return max < eps;
}

int mm_solve(Grid *g, AFMParams *p, double eps, const OutputStr *ostr) {
    double alpha = 0.01;

    double *l_new[3];
    double energy, energy_prev, max_bq_val;
    size_t i, j = 0, iter = 0, max_iter = 1000000;
    for (i = 0; i < 3; ++i)
        l_new[i] = (double *) malloc(g->N * sizeof(double));

    //if (ostr != NULL)
    //    plot_dist(g);

    while (mm_criterion(g, eps, &max_bq_val) || iter < max_iter) {
        //++j;
        first_der(g);
        second_der(g);
        h_eff(g, p);
        energy = afm_energy(g, p);
        berkov_step(l_new, g, alpha);

        mm_show_progress(iter, alpha, energy, max_bq_val);
        alpha = (iter + 1)%5000 ? alpha : 0.8*alpha;
        //if (((int) j) % 1000 == 0 && iter && energy > energy_prev) {
        //    alpha *= 0.5;
        //    continue;
        //}
        //if (((int) j) % 1000 == 0)
        //    alpha *= 2.0;

        for (i = 0; i < 3; ++i)
            memcpy(g -> l[i], l_new[i], g->N * sizeof(double));
        grid_norm_l(g);

        if (ostr != NULL && (iter + 1)%1000 == 0)
            plot_dist(g);

        energy_prev = energy;
        ++iter;
    }

    for (i = 0; i < 3; ++i)
        free(l_new[i]);

    return 0;
}

void mm_show_progress(size_t iter, double alpha, double energy,
        double max_bq_val) {
    printf("%7lu | %9f | %11.5e | %11.5e\n", iter, alpha, energy, max_bq_val); 
}

int output_init(OutputStr *s, const size_t output_iter,
        int (*plot)(const Grid *)) {
    if (output_iter > 0 || plot == NULL)
        return 1;

    s->output_iter = output_iter;
    s->plot = plot;

    return 0;
}
