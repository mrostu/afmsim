#include <plplot/plplot.h>
#include <math.h>
#include "plots.h"
#include "micromagn.h"

int nn_interp(const Grid *g, PLFLT **lx, PLFLT **ly, PLFLT **lz,
        const size_t nix, const size_t niy) {
    const double dx = g->dx,
                 dy = g->dy,
                 xmin = 0,
                 xmax = g->hx,
                 ymin = 0,
                 ymax = g->hx;
    const size_t nx = g->Nx,
                 ny = g->Ny;
    double x, y, xi, yi,
           dxi = xmax / nix,
           dyi = ymax / niy;
    size_t i1, i2, i0 = 0, j1, j2, j0 = 0;

    for (i1 = 0; i1 < nix; ++i1) {
        xi = 0.5*dxi + i1*dxi;
        for (i2 = i0; i2 < nx; ++i2) {
            x = 0.5*dx + i2*dx;
            if (xi >= x - 0.5*dx && xi < x + 0.5*dx) {
                i0 = i2;
                j0 = 0;
                for (j1 = 0; j1 < niy; ++j1) {
                    yi = 0.5*dyi + j1*dyi;
                    for (j2 = j0; j2 < ny; ++j2) {
                        y = 0.5*dy + j2*dy;
                        if (yi >= y - 0.5*dy && yi < y + 0.5*dy) {
                            lx[i1][j1] = g->l[0][ny*i2+j2];
                            ly[i1][j1] = g->l[1][ny*i2+j2];
                            lz[i1][j1] = 1 - fabs(g->l[2][ny*i2+j2]);
                            j0 = j2;
                            break;
                        }
                    }
                }
                break;
            }
        }
    }

    return 0;
}

int plot_dist(const Grid *g) {
    size_t i, j;
    const size_t nx = 51;
    const size_t ny = 51;
    const PLFLT xmin = 0;
    const PLFLT xmax = (const PLFLT) g->hx;
    const PLFLT ymin = 0;
    const PLFLT ymax = (const PLFLT) g->hy;
    PLFLT dx = (xmax - xmin) / nx;
    PLFLT dy = (ymax - ymin) / ny;

    PLFLT **lx, **ly, **lz;
    PLcGrid2 cgrid2;
    const int nc = 51;
    PLFLT clev[nc];

    plAlloc2dGrid(&cgrid2.xg, nx, ny);
    plAlloc2dGrid(&cgrid2.yg, nx, ny);
    plAlloc2dGrid(&lx, nx, ny);
    plAlloc2dGrid(&ly, nx, ny);
    plAlloc2dGrid(&lz, nx, ny);

    cgrid2.nx = nx;
    cgrid2.ny = ny;

    nn_interp(g, lx, ly, lz, nx, ny);

    for (i = 0; i < nx; ++i)
        for (j = 0; j < ny; ++j) {
            cgrid2.xg[i][j] = 0.5*dx + dx*i;
            cgrid2.yg[i][j] = 0.5*dy + dy*j;
        }

    for (i = 0; i < nc; ++i)
        clev[i] = -0.5 + 1.5*(PLFLT) i / (PLFLT) (nc - 1);

    //plbop();
    //pladv(0);
    plcol0(15);
    plenv(xmin, xmax, ymin, ymax, 0, 0);
    //plvpor(0.05, 0.95, 0.05, 0.95);
    //plwind(xmin, xmax, ymin, ymax);
    plshades((PLFLT_MATRIX) lz, nx, ny, NULL,
            xmin, xmax, ymin, ymax,
            clev, nc, 0.0, 0, 0.0, plfill, 1, NULL, NULL);
    plcol0(15);
    plvect((PLFLT_MATRIX) lx, (PLFLT_MATRIX) ly, nx, ny, 0.0, pltr2,
            (void *) &cgrid2);

    plFree2dGrid(cgrid2.xg, nx, ny);
    plFree2dGrid(cgrid2.yg, nx, ny);
    plFree2dGrid(lx, nx, ny);
    plFree2dGrid(ly, nx, ny);
    plFree2dGrid(lz, nx, ny);

    return 0;
}

int plot_test() {
    int i, j;
    PLFLT dx, dy, x, y;
    PLcGrid2 cgrid2;
    PLFLT **u, **v;
    const int nx = 20;
    const int ny = 20;
    PLFLT xmin, xmax, ymin, ymax;

    dx = 1.0;
    dy = 1.0;

    xmin = - nx / 2 * dx;
    xmax = nx / 2 * dx;
    ymin = - ny / 2 * dy;
    ymax = ny / 2 * dy;

    plAlloc2dGrid(&cgrid2.xg, nx, ny);
    plAlloc2dGrid(&cgrid2.yg, nx, ny);
    plAlloc2dGrid(&u, nx, ny);
    plAlloc2dGrid(&v, nx, ny);

    cgrid2.nx = nx;
    cgrid2.ny = ny;

    for (i = 0; i < nx; ++i) {
        x = (i - nx/2 + 0.5) * dx;
        for (j = 0; j < ny; ++j) {
            y = (j - ny/2 + 0.5) * dy;
            cgrid2.xg[i][j] = x;
            cgrid2.yg[i][j] = y;
            u[i][j] = y;
            v[i][j] = -x;
        }
    }

    //plbop();
    //pladv(0);
    plenv(xmin, xmax, ymin, ymax, 0, 0);
    pllab("(x)", "(y)", "circulation");
    plcol0(2);
    plvect((PLFLT_MATRIX) u, (PLFLT_MATRIX) v, nx, ny, 0.0, pltr2,
            (void *) &cgrid2);
    plcol0(1);

    plFree2dGrid(cgrid2.xg, nx, ny);
    plFree2dGrid(cgrid2.yg, nx, ny);
    plFree2dGrid(u, nx, ny);
    plFree2dGrid(v, nx, ny);

    return 0;
}
