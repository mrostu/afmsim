#ifndef plots_h
#define plots_h

#include <plplot/plplot.h>
#include "micromagn.h"

int nn_interp(const Grid *g, PLFLT **lx, PLFLT **ly, PLFLT **lz,
        size_t nix, size_t niy);
int plot_test();
int plot_dist(const Grid *g);

#endif
