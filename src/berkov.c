#include <stdlib.h>
#include "micromagn.h"

#define DOT3(x, y, i) x[0][i]*y[0][i] + x[1][i]*y[1][i] + x[2][i]*y[2][i]

void berkov_step(double *l_new[3], const Grid *g, double alpha) {
    size_t i;
    double d;
    for (i = 0; i < g -> N; ++i) {
        d = DOT3(g->l, g->Heff, i);
        l_new[0][i] = g->l[0][i] - alpha * (g->l[0][i] * d - g->Heff[0][i]);
        l_new[1][i] = g->l[1][i] - alpha * (g->l[1][i] * d - g->Heff[1][i]);
        l_new[2][i] = g->l[2][i] - alpha * (g->l[2][i] * d - g->Heff[2][i]);
    }
}
