#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <plplot/plplot.h>

#include "micromagn.h"
#include "plots.h"

#define PI 4*atan(1.0)
#define PLOT_ENABLED

void init_params(AFMParams *p);
void test_plots();

int main(int argc, char **argv) {
#ifdef PLOT_ENABLED
    plsetopt("geometry", "1024x768");
    plsetopt("np", 0);
    plsdev("xcairo");
    plspal0("cmap0_white_bg.pal");
    plspal1("cmap1_gray.pal", 1);
    plinit();
#endif
    //test_plots();

    //return 0;
    OutputStr ostr = {0, NULL};
    output_init(&ostr, 100, plot_dist);
    //plot_test();
    Grid *grid;
    AFMParams params;
    init_params(&params);
    if ((grid = grid_init(10, 10, 0.1, 0.1)) == NULL) {
        perror("Can't allocate memory for Grid varible!\n");
        exit(1);
    }
    if (grid_rand_dist(grid)) {
        perror("Can't create an initial distribution!\n");
        exit(1);
    }
    //double l0[3] = {0.9, 0.1, 0.1};
    //if (grid_uniform_dist(grid, l0)) {
    //    perror("Can't create an initial distribution!\n");
    //    exit(1);
    //}

    grid_norm_l(grid);
    grid_print_info(grid);
    mm_solve(grid, &params, 1e-3, &ostr);
    grid_free(grid);
#ifdef PLOT_ENABLED
    plend();
#endif
    return 0;
}

void init_params(AFMParams *p) {
    double A, chi, M0, Hd, beta, Ku;
    A = 2.8e-7;
    beta = 0.78;
    Ku = 1e6;
    Hd = 1.2e5;
    chi = 5.6e-5;
    M0 = 442;

    p -> kappaC = 4 * A * Ku / (beta*beta);
    p -> kappaD = 2 * A * chi * Hd*Hd / (beta*beta);
    p -> kappaM = 2 * A * chi * M0*M0 / (beta*beta);
    p -> hx     = 0;
    p -> hy     = 0;
    p -> hz     = 350;
}

void test_plots() {
    plsetopt("geometry", "1024x768");
    plsetopt("np", 0);
    plsdev("xwin");
    plspal0("cmap0_white_bg.pal");
    plspal1("cmap1_gray.pal", 1);
    plinit();

    Grid *g = grid_init(1, 1, 0.01, 0.01); 
    double l0[3] = {0.9, 0.1, 0.1};
    grid_uniform_dist(g, l0);
    grid_norm_l(g);
    grid_print_info(g);
    plot_dist(g);

    getchar();
    grid_free(g);
    plend();
}
