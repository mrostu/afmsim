all:
	gcc -Wall -O3 -lm -lgsl -lgslcblas -lplplot -o afmsim src/main.c src/micromagn.c src/berkov.c src/plots.c
plot_test:
	gcc -lm -lplplot -o plot_test src/plot_test.c
clean:
	rm -rv afmsim
